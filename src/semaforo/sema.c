#include "sema.h"

void P(struct Semaphore *S){
  while(OPA_cas_int(&S->mutex, 0, 1)){}
  OPA_decr_int(&S->value);
  if(OPA_load_int(&S->value) < 0){
    OPA_store_int(&S->mutex, 0);
  }else{
    OPA_store_int(&S->mutex, 0);
  }
}


void V(struct Semaphore *S){
  OPA_incr_int(&S->value);
  if(OPA_load_int(&S->value) <= 0){
    OPA_store_int(&S->mutex, 0);
  }else{
    OPA_store_int(&S->mutex, 0);
  } 
}

struct Semaphore * iniciaStruct(struct Semaphore *S, const unsigned int n){
  S = (struct Semaphore*)malloc(sizeof(struct Semaphore));
  OPA_store_int(&S->value, n);
  OPA_store_int(&S->mutex, 0);
  
  return S;
}