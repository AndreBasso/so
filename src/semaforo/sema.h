#ifndef SEMA_H
#define	SEMA_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <opa_primitives.h>

    
struct Semaphore{
    OPA_int_t value;
    OPA_int_t mutex;
};

struct Semaphore * iniciaStruct(struct Semaphore *S, const unsigned int n);
void P(struct Semaphore *S);
void V(struct Semaphore *S);



#ifdef	__cplusplus
}
#endif

#endif	/* SEMA_H */
