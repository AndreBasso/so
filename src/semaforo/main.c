#include "sema.h"
#define N 10

int buffer[10];
int i = 0;
int j = 0;
int x,y;

struct Semaphore *empty, *full, *mutex;

void * produtor() {
    for(x=0;x<50;x++){
        P(empty);
        P(mutex);
        i=(i+1)%N;
        buffer[i]= 1;
        printf("Produtor produziu %d\n", i);
        V(mutex);
        V(full);
    }
}

void * consumidor() {
    for (y=0;y<50;y++) {
        P(full);
        P(mutex);
        j=(j+1)%N;
        buffer[j]=0;
        printf("Consumidor consumiu %d\n", j);
        V(mutex);
        V(empty);
    }
}

int main() {    
    int iret1, iret2;
    pthread_t t0, t1;
    
    empty = iniciaStruct(empty, N);
    full = iniciaStruct(full, 0);
    mutex = iniciaStruct(mutex, 1);
    
    iret1 = pthread_create(&t0, NULL, produtor, NULL);
    iret2 = pthread_create(&t1, NULL, consumidor, NULL);
    
    pthread_join(t0, NULL);
    pthread_join(t1, NULL);
    
    return 0;
}
